package com.testinglab.testinglab.controllers;

import com.testinglab.testinglab.model.User;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserIntegrationTest {

    @LocalServerPort
    private int port;

    private String baseUrl;

    @Autowired
    private TestRestTemplate template;

    @Before
    public void setUp() throws Exception {
        baseUrl = "http://localhost:" + port + "/users/";
    }

    @BeforeClass
    public static void connect() {

    }

    @Test
    public void getAllUsers() {
        ArrayList<User> result =
                template.exchange(baseUrl,
                        HttpMethod.GET, null, new ParameterizedTypeReference<ArrayList<User>>() {
                        }).getBody();

        ArrayList<User> temp = new ArrayList<>();
        temp.add(User.builder()
                .id(14L)
                .firstName("Mishik")
                .surname("Sosnin")
                .age(24)
                .sex("male")
                .build());

        temp.add(User.builder()
                .id(13L)
                .firstName("Nasta")
                .surname("Romash")
                .age(21)
                .build());



        assertThat(result, equalTo(temp));
    }

    @Test
    public void getUserById() {
        Long id = 13L;
        ResponseEntity<User> result = template.getForEntity(baseUrl + id, User.class);
        User temp = User.builder()
                .id(13L)
                .firstName("Nasta")
                .surname("Romash")
                .age(21)
                .build();
        assertThat(result.getBody(), equalTo(temp));
    }

    @Test
    public void createUser() {
        User temp = User.builder()
                .firstName("Piter")
                .surname("Petrov")
                .sex("male")
                .age(19)
                .build();
        ResponseEntity<User> result = template.postForEntity(baseUrl, temp, User.class);
        temp.setId(15L);
        assertThat(result.getBody(), equalTo(temp));
    }

    @Test
    public void updateUser() {
        User temp = User.builder()
                .firstName("Fedya")
                .surname("Petrov")
                .sex("male")
                .age(22)
                .build();
        Long id = 15L;

    }

    @Test
    public void deleteUser() {
        Long id = 16L;
        template.delete(baseUrl + id);
        ResponseEntity<User> result = template.getForEntity(baseUrl + id, User.class);
        assertThat(result.getBody(), equalTo(null));
    }

    private User buildUser(String name) {
        return User.builder()
                .id(1L)
                .firstName(name)
                .surname("Bond")
                .age(41)
                .sex("male")
                .country("USA")
                .build();
    }

}