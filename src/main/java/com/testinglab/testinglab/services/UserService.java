package com.testinglab.testinglab.services;

import com.testinglab.testinglab.model.User;
import com.testinglab.testinglab.repos.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }

    public Optional<User> getUserById(Long userId) {
        return userRepository.findById(userId);
    }

    public User createUser(User user) {
        return userRepository.save(user);
    }

//    public User updateUser(Long userId, User user) {
//        return service.updateUserById(userId, user);
////        userRepository.
//    }userRepository

    public void deleteUser(Long userId) {
        userRepository.deleteById(userId);
    }
}
