package com.testinglab.testinglab.controllers;

import com.testinglab.testinglab.model.User;
import com.testinglab.testinglab.repos.UserRepository;
import com.testinglab.testinglab.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = "/users")
public class UserController {
    @Autowired
    private UserService service;

    @Autowired
    private UserRepository userRepository;

    @GetMapping
    public Iterable<User> getAllUsers() {
//        return userRepository.findAll();
        return service.getAllUsers();
    }

    @GetMapping("{userId}")
    public Optional<User> getUserById(@PathVariable Long userId) {
        return service.getUserById(userId);
//        return userRepository.findById(userId);
    }

    @PostMapping
    public User createUser(@RequestBody User user) {
//        return userRepository.save(user);'
        return service.createUser(user);
    }

//    @PutMapping("{userId}")
//    public User updateUser(@PathVariable Long userId,
//                           @RequestBody User user) {
//        return service.updateUserById(userId, user);
////        userRepository.
//    }

    @DeleteMapping("{userId}")
    public void deleteUser(@PathVariable Long userId) {
//        userRepository.deleteById(userId);
        service.deleteUser(userId);
    }
}
